# tfm_arduino_node_simulation

:exclamation: Please, to use this repository start with doing the setup defined in https://gitlab.com/tfm_3dprinting_sensors/tfm_workspace_setup.

## Repository structure

This repository contains a Simulation of what the [Arduino ROS node](https://gitlab.com/tfm_3dprinting_sensors/tfm_arduino_node) would broadcast with real harware connected to the sensors. The real signal data is streamed in a defined rate and there is no need to have the real hardware connected to experiment with the perception and control.

## Authors and acknowledgment
Oriol Vendrell

## License
BSD 2-Clause "Simplified" License

## Project status
This project is part of my final master's thesis.
