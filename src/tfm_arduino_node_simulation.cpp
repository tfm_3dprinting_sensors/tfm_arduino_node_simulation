#include "tfm_signal_sim.h"

int main(int argc, char **argv)
{

  ros::init(argc, argv, "perception");
 
  SignalController signal(argc, argv);

  ros::Rate loop_rate(50);
  while (ros::ok())
  {
    signal.readSensorSignal();
    signal.publishSensorSignal();

    ros::spinOnce();

    loop_rate.sleep();
  }

  return 0;
}
