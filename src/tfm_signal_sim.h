#ifndef SIGNAL_CONTROLLER_H_
#define SIGNAL_CONTROLLER_H_

#include "ros/ros.h"
#include "std_msgs/Float64.h"
#include "geometry_msgs/Twist.h"
#include <cmath>
#include <vector>
#include <fstream>
#include <iterator>
#include <iostream>
#include <sstream>
#include <string>

class SignalController
{
    private:
      
      // node handle
      ros::NodeHandle n_;

      // Publishers
      ros::Publisher sensor1_pub_;
      ros::Publisher sensor2_pub_;
    
      // atributes
      std_msgs::Float64 sensor1_;
      std_msgs::Float64 sensor2_;

      std::vector<float> sensor1_data_;
      std::vector<float> sensor2_data_;

      int sensor1_it_count_;
      int sensor2_it_count_;
      
      // methods

    public:
     
      // public methods
      void readSensorSignal();
      void publishSensorSignal();

      // constructor
      SignalController(int argc, char**argv);
   
};

#endif
